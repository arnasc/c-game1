#include "StaticSprite.h"
#include "DataTables.hpp"
#include "Utility.hpp"
#include "Pickup.hpp"
#include "CommandQueue.hpp"
#include "SoundNode.hpp"
#include "NetworkNode.hpp"
#include "ResourceHolder.hpp"

#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderStates.hpp>

#include <cmath>

namespace
{
	const std::vector<StaticSpriteData> Table = initializeStaticSpriteData();
}

StaticSprite::StaticSprite(Type type, const TextureHolder& textures) 
	: Entity(100)
	, mSprite(textures.get(Table[type].texture)
	, Table[type].textureRect)
	, mType(type)
{
	if (mType == StaticSprite::WindowCharacter) {
		centerOrigin(mSprite);
	}
	if (mType == StaticSprite::Sirens) {
		anim = new Animation(textures.get(Textures::Sirens));
		anim->setFrameSize(sf::Vector2i(26, 15));
		anim->setNumFrames(4);
		anim->setDuration(sf::seconds(.3f));
		anim->setRepeating(true);
		centerOrigin(*anim);
	}
}


void StaticSprite::drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const
{
	if (anim != NULL) {
		target.draw(*anim, states);
	}
	target.draw(mSprite, states);
}

void StaticSprite::updateCurrent(sf::Time dt, CommandQueue& commands)
{
	if (mType == StaticSprite::Gun) {
		RotateTowardsMouse();
	}
	if (anim != NULL) {
		anim->update(dt);
	}
	

	Entity::updateCurrent(dt, commands);
}

void StaticSprite::RotateTowardsMouse() {
	sf::Vector2f curPos = getWorldPosition();
	float dx = curPos.x - mouseTarget.x;
	float dy = curPos.y - mouseTarget.y;
	float rotation = (atan2(dy, dx)) * 180 / PI;
	setRotation(rotation + 180);
}

void StaticSprite::damage(int point)
{
	return;
}

StaticSprite::~StaticSprite()
{
	delete anim;
	anim = NULL;
}
