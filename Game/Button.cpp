#include "Button.hpp"
#include "Utility.hpp"
#include "SoundPlayer.hpp"
#include "ResourceHolder.hpp"

#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/RenderStates.hpp>
#include <SFML/Graphics/RenderTarget.hpp>

namespace GUI
{

	Button::Button(State::Context context)
		: mCallback()
		, mSprite(context.textures->get(Textures::Buttons))
		, mText("", context.fonts->get(Fonts::Main), 16)
		, mIsToggle(false)
		, mSounds(*context.sounds)
	{
		sf::FloatRect bounds = mSprite.getLocalBounds();
		//mText.setPosition(bounds.width / 2.f, bounds.height / 2.f);
	}

	void Button::setCallback(Callback callback)
	{
		mCallback = std::move(callback);
	}

	void Button::setText(const std::string& text)
	{
		mText.setString(text);
		//centerOrigin(mText);
	}

	void Button::setToggle(bool flag)
	{
		mIsToggle = flag;
	}


	bool Button::isSelectable() const
	{
		return true;
	}

	void Button::checkSelectedTexture(std::string text)
	{
		if (mText.getString() == "Play")
		{
			changeTexture(PlaySelected);
		}
		else if (mText.getString() == "Settings")
		{
			changeTexture(SettingsSelected);
		}
		else if (mText.getString() == "Sound")
		{
			changeTexture(SoundSelected);
		}
		else if (mText.getString() == "Music")
		{
			changeTexture(MusicSelected);
		}
		else if (mText.getString() == "Back")
		{
			changeTexture(BackSelected);
		}
		else
		{
			changeTexture(QuitSelected);
		}
	}

	void Button::checkNormalTexture(std::string text)
	{
		if (mText.getString() == "Play")
		{
			changeTexture(PlayNormal);
		}
		else if (mText.getString() == "Settings")
		{
			changeTexture(SettingsNormal);
		}
		else if (mText.getString() == "Sound")
		{
			changeTexture(SoundNormal);
		}
		else if (mText.getString() == "Music")
		{
			changeTexture(MusicNormal);
		}
		else if (mText.getString() == "Back")
		{
			changeTexture(BackNormal);
		}
		else
		{
			changeTexture(QuitNormal);
		}
	}

	void Button::select()
	{
		Component::select();
		checkSelectedTexture(mText.getString());
		
	}

	void Button::deselect()
	{
		Component::deselect();
		checkNormalTexture(mText.getString());
	}

	void Button::activate()
	{
		Component::activate();

		// If we are toggle then we should show that the button is pressed and thus "toggled".
		if (mIsToggle) {
			checkSelectedTexture(mText.getString());
		}

		if (mCallback)
			mCallback();

		// If we are not a toggle then deactivate the button since we are just momentarily activated.
		if (!mIsToggle)
			deactivate();

		mSounds.play(SoundEffect::Button);
	}

	void Button::deactivate()
	{
		Component::deactivate();

		if (mIsToggle)
		{
			// Reset texture to right one depending on if we are selected or not.
			if (isSelected())
			{
				checkSelectedTexture(mText.getString());
			}
			else
			{
				checkNormalTexture(mText.getString());
			}
		}
	}

	void Button::handleEvent(const sf::Event&)
	{
	}

	void Button::draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
		states.transform *= getTransform();
		target.draw(mSprite, states);
		//target.draw(mText, states);
	}

	void Button::changeTexture(Type buttonType)
	{
		sf::IntRect textureRect(0, 125 * buttonType, 500, 125);
		mSprite.setTextureRect(textureRect);
	}
}