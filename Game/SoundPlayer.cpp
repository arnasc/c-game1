#include "SoundPlayer.hpp"

#include <SFML/Audio/Listener.hpp>

#include <cmath>

namespace
{
	//Sound coordinate system, point of view of the player in front of the screen
	//X = left, Y = up, Z = (out of the screen)
	const float ListenerZ = 300.f;
	const float Attenuation = 8.f;
	const float MinDistance2D = 200.f;
	const float MinDistance3D = std::sqrt(MinDistance2D * MinDistance2D + ListenerZ * ListenerZ);
}


extern bool soundMuted;

SoundPlayer::SoundPlayer()
	:mSoundBuffers()
	, mSounds()
	, volumeMap()
{
	mSoundBuffers.load(SoundEffect::AlliedGunfire, "Media/Sound/AlliedGunfire.wav");
	mSoundBuffers.load(SoundEffect::EnemyGunfire, "Media/Sound/EnemyGunfire.wav");
	mSoundBuffers.load(SoundEffect::Explosion1, "Media/Sound/Explosion1.wav");
	mSoundBuffers.load(SoundEffect::Explosion2, "Media/Sound/Explosion2.wav");
	mSoundBuffers.load(SoundEffect::LaunchMissile, "Media/Sound/LaunchMissile.wav");
	mSoundBuffers.load(SoundEffect::CollectPickup, "Media/Sound/CollectPickup.wav");
	mSoundBuffers.load(SoundEffect::Button, "Media/Sound/Button.wav");
	mSoundBuffers.load(SoundEffect::HitMarker, "Media/Sound/HitMarker.wav");
	mSoundBuffers.load(SoundEffect::VoiceChewGum, "Media/Sound/VoiceChewGum.wav");
	mSoundBuffers.load(SoundEffect::VoiceEatShit, "Media/Sound/VoiceEatShit.wav");
	mSoundBuffers.load(SoundEffect::VoiceJusticeServed, "Media/Sound/VoiceJusticeServed.wav");
	mSoundBuffers.load(SoundEffect::VoiceMaxJustice, "Media/Sound/VoiceMaxJustice.wav");
	mSoundBuffers.load(SoundEffect::VoiceServed, "Media/Sound/VoiceServed.wav");
	mSoundBuffers.load(SoundEffect::VoiceTasteJustice, "Media/Sound/VoiceTasteJustice.wav");
	mSoundBuffers.load(SoundEffect::VioceCrime, "Media/Sound/VioceCrime.wav");
	mSoundBuffers.load(SoundEffect::VoiceCapital, "Media/Sound/VoiceCapital.wav");
	mSoundBuffers.load(SoundEffect::VoiceJudge, "Media/Sound/VoiceJudge.wav");
	mSoundBuffers.load(SoundEffect::VoiceJusticeMax, "Media/Sound/VoiceJusticeMax.wav");
	mSoundBuffers.load(SoundEffect::VoiceSmell, "Media/Sound/VoiceSmell.wav");
	mSoundBuffers.load(SoundEffect::VoiceTasteGood, "Media/Sound/VoiceTasteGood.wav");
	mSoundBuffers.load(SoundEffect::Helicopter, "Media/Sound/Helicopter.ogg");

	if (soundMuted) {
		volumeMap[SoundEffect::EnemyGunfire] = 0;
		volumeMap[SoundEffect::AlliedGunfire] = 0;
		volumeMap[SoundEffect::HitMarker] = 0;
		volumeMap[SoundEffect::Helicopter] = 0;
		volumeMap[SoundEffect::Explosion1] = 0;
		volumeMap[SoundEffect::Explosion2] = 0;
	}
	else {
		volumeMap[SoundEffect::EnemyGunfire] = 30;
		volumeMap[SoundEffect::AlliedGunfire] = 30;
		volumeMap[SoundEffect::Helicopter] = 60;
		volumeMap[SoundEffect::HitMarker] = 50;
		volumeMap[SoundEffect::Explosion1] = 200;
		volumeMap[SoundEffect::Explosion2] = 200;
	}

	//Listener points towards the screen
	sf::Listener::setDirection(0.f, 0.f, -1.f);
}

void SoundPlayer::stop()
{

	
}

void SoundPlayer::play(SoundEffect::ID effect)
{
	play(effect, getListenerPosition()); //e.g sound for a button
}

void SoundPlayer::play(SoundEffect::ID effect, sf::Vector2f position)
{
	play(effect, position, false);
}

void SoundPlayer::play(SoundEffect::ID effect, sf::Vector2f position, bool loop)
{
	mSounds.push_back(sf::Sound());
	sf::Sound& sound = mSounds.back();



	sound.setBuffer(mSoundBuffers.get(effect));
	sound.setPosition(position.x, -position.y, 0.f);
	sound.setAttenuation(Attenuation);
	sound.setMinDistance(MinDistance3D);
	if (soundMuted) {
		sound.setVolume(0);
	}
	else if (volumeMap.find(effect) != volumeMap.end()) {
		sound.setVolume(volumeMap[effect]);
	}
	else {
		sound.setVolume(100);
	}
	sound.play();    //sound for explosions or enemy gunfire
}

void SoundPlayer::removeStoppedSounds()
{
	mSounds.remove_if([](const sf::Sound& s)
	{
		return s.getStatus() == sf::Sound::Stopped;
	});
}

void SoundPlayer::setListenerPosition(sf::Vector2f position)
{
	sf::Listener::setPosition(position.x, -position.y, ListenerZ);
}

sf::Vector2f SoundPlayer::getListenerPosition() const
{
	sf::Vector3f position = sf::Listener::getPosition(); //returns a 3D vectors but Z is fixed
	return sf::Vector2f(position.x, -position.y);
}





