#pragma once
#include "Entity.hpp"
#include <SFML\Graphics\Sprite.hpp>
#include "ResourceIdentifiers.hpp"
#include "Animation.hpp"

extern sf::Vector2f mouseTarget;
class StaticSprite : public Entity{
public:
	enum Type {
		WindowCharacter,
		Gun,
		Sirens,
		TypeCount
	};

	StaticSprite(Type type, const TextureHolder& textures);

	Type			mType;
	sf::Sprite		mSprite;
	Animation*		anim = NULL;
	const float		PI = 3.14159265;

	void	drawCurrent(sf::RenderTarget & target, sf::RenderStates states) const;
	void	updateCurrent(sf::Time dt, CommandQueue & commands);
	void	RotateTowardsMouse();
	void	damage(int point);

	~StaticSprite();
};