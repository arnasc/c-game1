#include "World.hpp"
#include "Projectile.hpp"
#include "Pickup.hpp"
#include "Foreach.hpp"
#include "TextNode.hpp"
#include "ParticleNode.hpp"
#include "SoundNode.hpp"
#include "NetworkNode.hpp"
#include "Utility.hpp"
#include "StaticSprite.h"
#include "SoundPlayer.hpp"

#include <SFML/Graphics/RenderTarget.hpp>


#include <algorithm>
#include <cmath>
#include <limits>

int enemiesKilled;
int numEnemies;
extern int gameScore;
sf::Vector2f mouseTarget;

World::World(sf::RenderTarget& outputTarget, FontHolder& fonts, SoundPlayer& sounds, bool networked)
	: mTarget(outputTarget)
	, mSceneTexture()
	, mWorldView(outputTarget.getDefaultView())
	, mTextures()
	, mFonts(fonts)
	, mSounds(sounds)
	, mSceneGraph()
	, mSceneLayers()
	, mWorldBounds(0.f, 0.f, mWorldView.getSize().x, 80000.f)
	, mSpawnPosition(mWorldView.getSize().x / 2.f, mWorldBounds.height - mWorldView.getSize().y / 2.f)
	, mScrollSpeed(-300.f)
	, mScrollSpeedCompensation(1.1f)
	, mPlayerAircrafts()
	, mEnemySpawnPoints()
	, mActiveEnemies()
	, mNetworkedWorld(networked)
	, mNetworkNode(nullptr)
	, mFinishSprite(nullptr)
	, spawnOffset(0.f)
{
	enemiesKilled = 0;
	gameScore = 0;
	mSceneTexture.create(mTarget.getSize().x, mTarget.getSize().y);

	loadTextures();
	buildScene();
	// Prepare the view
	mWorldView.setCenter(mSpawnPosition);
}

void World::setWorldScrollCompensation(float compensation)
{
	mScrollSpeedCompensation = compensation;
}

void World::update(sf::Time dt)
{
	if (enemiesKilled >= numEnemies) {
		createMoreEnemies(abs(spawnOffset));
		sortEnemies();
		enemiesKilled = 0;
	}

	// Scroll the world, reset player velocity
	float moveY = mScrollSpeed * dt.asSeconds() * mScrollSpeedCompensation;
	mWorldView.move(0.f, moveY);
	spawnOffset += moveY;
	
	FOREACH(Vehicle* a, mPlayerAircrafts)
		a->setVelocity(0.f, 0.f);

	// Setup commands to destroy entities, and guide missiles
	destroyEntitiesOutsideView();
	//guideMissiles();

	// Forward commands to scene graph, adapt velocity (scrolling, diagonal correction)
	while (!mCommandQueue.isEmpty())
		mSceneGraph.onCommand(mCommandQueue.pop(), dt);
	adaptPlayerVelocity();

	// Collision detection and response (may destroy entities)
	handleCollisions();

	// Remove aircrafts that were destroyed (World::removeWrecks() only destroys the entities, not the pointers in mPlayerAircraft)
	auto firstToRemove = std::remove_if(mPlayerAircrafts.begin(), mPlayerAircrafts.end(), std::mem_fn(&Vehicle::isMarkedForRemoval));


	mPlayerAircrafts.erase(firstToRemove, mPlayerAircrafts.end());

	// Remove all destroyed entities, create new ones
	mSceneGraph.removeWrecks();
	spawnEnemies();

	// Regular update step, adapt position (correct if outside view)
	mSceneGraph.update(dt, mCommandQueue);
	adaptPlayerPosition();
	updateMousePosition();
	updateSounds();
}

void World::draw()
{
	if (PostEffect::isSupported())
	{
		mSceneTexture.clear();
		mSceneTexture.setView(mWorldView);
		mSceneTexture.draw(mSceneGraph);
		mSceneTexture.display();
		mBloomEffect.apply(mSceneTexture, mTarget);
	}
	else
	{
		mTarget.setView(mWorldView);
		mTarget.draw(mSceneGraph);
	}
}

CommandQueue& World::getCommandQueue()
{
	return mCommandQueue;
}

Vehicle* World::getAircraft(int identifier) const
{
	FOREACH(Vehicle* a, mPlayerAircrafts)
	{
		if (a->getIdentifier() == identifier)
			return a;
	}

	return nullptr;
}

void World::removeAircraft(int identifier)
{
	Vehicle* aircraft = getAircraft(identifier);
	if (aircraft)
	{
		aircraft->destroy();
		mPlayerAircrafts.erase(std::find(mPlayerAircrafts.begin(), mPlayerAircrafts.end(), aircraft));
	}
}

Vehicle* World::addAircraft(int identifier)
{
	std::unique_ptr<Vehicle> player(new Vehicle(Vehicle::Max, mTextures, mFonts));
	player->setPosition(mWorldView.getCenter());
	player->setIdentifier(identifier);

	std::unique_ptr<StaticSprite> windowCharacter(new StaticSprite(StaticSprite::WindowCharacter, mTextures));
	windowCharacter->setPosition(sf::Vector2f(15.f,-5.f));

	std::unique_ptr<StaticSprite> characterGun(new StaticSprite(StaticSprite::Gun, mTextures));
	characterGun->setPosition(sf::Vector2f(0.f, -3.f));
	characterGun->setOrigin(0, 2);

	std::unique_ptr<StaticSprite> sirens(new StaticSprite(StaticSprite::Sirens, mTextures));
	sirens->setPosition(sf::Vector2f(0, -4.f));

	windowCharacter->attachChild(std::move(characterGun));
	player->attachChild(std::move(windowCharacter));
	player->attachChild(std::move(sirens));


	mPlayerAircrafts.push_back(player.get());
	mSceneLayers[UpperAir]->attachChild(std::move(player));
	return mPlayerAircrafts.back();
}

void World::createPickup(sf::Vector2f position, Pickup::Type type)
{
	std::unique_ptr<Pickup> pickup(new Pickup(type, mTextures));
	pickup->setPosition(position);
	pickup->setVelocity(0.f, 1.f);
	mSceneLayers[UpperAir]->attachChild(std::move(pickup));
}

bool World::pollGameAction(GameActions::Action& out)
{
	return mNetworkNode->pollGameAction(out);
}

void World::setCurrentBattleFieldPosition(float lineY)
{
	mWorldView.setCenter(mWorldView.getCenter().x, lineY - mWorldView.getSize().y / 2);
	mSpawnPosition.y = mWorldBounds.height;
}

void World::setWorldHeight(float height)
{
	mWorldBounds.height = height;
}

bool World::hasAlivePlayer() const
{
	return mPlayerAircrafts.size() > 0;
}

bool World::hasPlayerReachedEnd() const
{
	if (Vehicle* aircraft = getAircraft(1))
		return !mWorldBounds.contains(aircraft->getPosition());
	else
		return false;
}

void World::loadTextures()
{
	mTextures.load(Textures::Entities, "Media/Textures/Entities.png");
	mTextures.load(Textures::Jungle, "Media/Textures/Jungle.png");
	mTextures.load(Textures::Explosion, "Media/Textures/Explosion.png");
	mTextures.load(Textures::PopText, "Media/Textures/PopText.png");
	mTextures.load(Textures::Particle, "Media/Textures/Particle.png");
	mTextures.load(Textures::Sirens, "Media/Textures/sirens.png");
	mTextures.load(Textures::FinishLine, "Media/Textures/FinishLine.png");
}

void World::adaptPlayerPosition()
{
	// Keep player's position inside the screen bounds, at least borderDistance units from the border
	sf::FloatRect viewBounds = getViewBounds();
	const float borderDistance = 40.f;

	FOREACH(Vehicle* aircraft, mPlayerAircrafts)
	{
		sf::Vector2f position = aircraft->getPosition();
		position.x = std::max(position.x, viewBounds.left + borderDistance);
		position.x = std::min(position.x, viewBounds.left + viewBounds.width - borderDistance);
		position.y = std::max(position.y, viewBounds.top + borderDistance);
		position.y = std::min(position.y, viewBounds.top + viewBounds.height - borderDistance);
		aircraft->setPosition(position);
	}
}

void World::adaptPlayerVelocity()
{
	FOREACH(Vehicle* aircraft, mPlayerAircrafts)
	{
		sf::Vector2f velocity = aircraft->getVelocity();

		// If moving diagonally, reduce velocity (to have always same velocity)
		if (velocity.x != 0.f && velocity.y != 0.f)
			aircraft->setVelocity(velocity / std::sqrt(2.f));

		// Add scrolling velocity
		aircraft->accelerate(0.f, mScrollSpeed);
	}
}

bool matchesCategories(SceneNode::Pair& colliders, Category::Type type1, Category::Type type2)
{
	unsigned int category1 = colliders.first->getCategory();
	unsigned int category2 = colliders.second->getCategory();

	// Make sure first pair entry has category type1 and second has type2
	if (type1 & category1 && type2 & category2)
	{
		return true;
	}
	else if (type1 & category2 && type2 & category1)
	{
		std::swap(colliders.first, colliders.second);
		return true;
	}
	else
	{
		return false;
	}
}

void World::handleCollisions()
{
	std::set<SceneNode::Pair> collisionPairs;
	mSceneGraph.checkSceneCollision(mSceneGraph, collisionPairs);

	FOREACH(SceneNode::Pair pair, collisionPairs)
	{
		if (matchesCategories(pair, Category::PlayerAircraft, Category::EnemyAircraft))
		{
			auto& player = static_cast<Vehicle&>(*pair.first);
			auto& enemy = static_cast<Vehicle&>(*pair.second);

			if (enemy.mType != Vehicle::Helicopter) {
				// Collision: Player damage = enemy's remaining HP
				player.damage(enemy.getHitpoints());
				enemy.destroy();
			}
		}

		else if (matchesCategories(pair, Category::PlayerAircraft, Category::Pickup))
		{
			auto& player = static_cast<Vehicle&>(*pair.first);
			auto& pickup = static_cast<Pickup&>(*pair.second);

			// Apply pickup effect to player, destroy projectile
			pickup.apply(player);
			pickup.destroy();
			player.playLocalSound(mCommandQueue, SoundEffect::CollectPickup);
		}

		else if (matchesCategories(pair, Category::EnemyAircraft, Category::AlliedProjectile)
			|| matchesCategories(pair, Category::PlayerAircraft, Category::EnemyProjectile))
		{
			auto& aircraft = static_cast<Vehicle&>(*pair.first);
			auto& projectile = static_cast<Projectile&>(*pair.second);

			// Apply projectile damage to aircraft, destroy projectile
			aircraft.damage(projectile.getDamage());
			mSounds.play(SoundEffect::HitMarker);
			projectile.destroy();
		}
	}
}

void World::updateSounds()
{
	sf::Vector2f listenerPosition;

	// 0 players (multiplayer mode, until server is connected) -> view center
	if (mPlayerAircrafts.empty())
	{
		listenerPosition = mWorldView.getCenter();
	}

	// 1 or more players -> mean position between all aircrafts
	else
	{
		FOREACH(Vehicle* aircraft, mPlayerAircrafts)
			listenerPosition += aircraft->getWorldPosition();

		listenerPosition /= static_cast<float>(mPlayerAircrafts.size());
	}

	// Set listener's position
	mSounds.setListenerPosition(listenerPosition);

	// Remove unused sounds
	mSounds.removeStoppedSounds();
}

void World::buildScene()
{
	// Initialize the different layers
	for (std::size_t i = 0; i < LayerCount; ++i)
	{
		Category::Type category = (i == LowerAir) ? Category::SceneAirLayer : Category::None;

		SceneNode::Ptr layer(new SceneNode(category));
		mSceneLayers[i] = layer.get();

		mSceneGraph.attachChild(std::move(layer));
	}

	// Prepare the tiled background
	sf::Texture& jungleTexture = mTextures.get(Textures::Jungle);
	jungleTexture.setRepeated(true);

	float viewHeight = mWorldView.getSize().y;
	sf::IntRect textureRect(mWorldBounds);
	textureRect.height += static_cast<int>(viewHeight);

	// Add the background sprite to the scene
	std::unique_ptr<SpriteNode> jungleSprite(new SpriteNode(jungleTexture, textureRect));
	jungleSprite->setPosition(mWorldBounds.left, mWorldBounds.top - viewHeight);
	mSceneLayers[Background]->attachChild(std::move(jungleSprite));

	// Add particle node to the scene
	std::unique_ptr<ParticleNode> smokeNode(new ParticleNode(Particle::Smoke, mTextures));
	mSceneLayers[LowerAir]->attachChild(std::move(smokeNode));

	// Add propellant particle node to the scene
	std::unique_ptr<ParticleNode> propellantNode(new ParticleNode(Particle::Propellant, mTextures));
	mSceneLayers[LowerAir]->attachChild(std::move(propellantNode));

	// Add bullet trace particle node to the scene
	std::unique_ptr<ParticleNode> dirtNode(new ParticleNode(Particle::Dirt, mTextures));
	mSceneLayers[LowerAir]->attachChild(std::move(dirtNode));

	//Add sound effect node
	std::unique_ptr<SoundNode> soundNode(new SoundNode(mSounds));
	mSceneGraph.attachChild(std::move(soundNode));

	// Add network node, if necessary
	if (mNetworkedWorld)
	{
		std::unique_ptr<NetworkNode> networkNode(new NetworkNode());
		mNetworkNode = networkNode.get();
		mSceneGraph.attachChild(std::move(networkNode));
	}

	// Add enemy aircraft
	addEnemies();
}

void World::createMoreEnemies(int offset = 0) {
	addEnemy(Vehicle::MuscleCar, 70.f, 800.f + offset);

	addEnemy(Vehicle::Biker, -200.f, 1000.f + offset);

	addEnemy(Vehicle::Helicopter, 0.f, 1000.f + offset);

	addEnemy(Vehicle::MuscleCar, -100.f, 1800.f + offset);

	addEnemy(Vehicle::MuscleCar, 400.f, 2700.f + offset);

	addEnemy(Vehicle::MuscleCar, 0.f, 3400.f + offset);

	addEnemy(Vehicle::MuscleCar, 200.f, 3700.f + offset);

	addEnemy(Vehicle::Helicopter, 0.f, 4400.f + offset);

	addEnemy(Vehicle::MuscleCar, 400.f, 5000.f + offset);

	addEnemy(Vehicle::MuscleCar, 200.f, 5500.f + offset);

	addEnemy(Vehicle::MuscleCar, 0.f, 5000.f + offset);

	addEnemy(Vehicle::Biker, 200.f, 5500.f + offset);

	numEnemies = 12;
}

void World::addEnemies()
{
	if (mNetworkedWorld)
		return;

	// Add enemies to the spawn point container
	createMoreEnemies();


	// Sort all enemies according to their y value, such that lower enemies are checked first for spawning
	sortEnemies();
}

void World::sortEnemies()
{
	// Sort all enemies according to their y value, such that lower enemies are checked first for spawning
	std::sort(mEnemySpawnPoints.begin(), mEnemySpawnPoints.end(), [](SpawnPoint lhs, SpawnPoint rhs)
	{
		return lhs.y < rhs.y;
	});
}

void World::addEnemy(Vehicle::Type type, float relX, float relY)
{
	SpawnPoint spawn(type, mSpawnPosition.x + relX, mSpawnPosition.y - relY);
	mEnemySpawnPoints.push_back(spawn);
}

void World::spawnEnemies()
{
	float top = getBattlefieldBounds().top;
	// Spawn all enemies entering the view area (including distance) this frame
	while (!mEnemySpawnPoints.empty() && (mEnemySpawnPoints.back().y) > (getBattlefieldBounds().top))
	{
		SpawnPoint spawn = mEnemySpawnPoints.back();

		std::unique_ptr<Vehicle> enemy(new Vehicle(spawn.type, mTextures, mFonts));

		if (spawn.type == Vehicle::Helicopter || spawn.type == Vehicle::Biker) {
			enemy->setPosition(spawn.x, getBattlefieldBounds().top + 100);
			enemy->setRotation(180.f);

		}
		else {
			enemy->setPosition(spawn.x, getBattlefieldBounds().top + getBattlefieldBounds().height + 100);
		}



		if (mNetworkedWorld) enemy->disablePickups();

		mSceneLayers[UpperAir]->attachChild(std::move(enemy));

		// Enemy is spawned, remove from the list to spawn
		mEnemySpawnPoints.pop_back();
	}
}

void World::destroyEntitiesOutsideView()
{
	Command command;
	command.category = Category::Projectile | Category::EnemyAircraft;
	command.action = derivedAction<Entity>([this](Entity& e, sf::Time)
	{
		Vehicle& a = static_cast<Vehicle&>(e);

		if (a.getMaxSpeed() < 0) {
			float y = a.getWorldPosition().y;
			float top = getBattlefieldBounds().top;
			if (a.getWorldPosition().y < getBattlefieldBounds().top) {
				e.remove();
			}
			return;
		};


		if (!getBattlefieldBounds().intersects(e.getBoundingRect())) {
			e.remove();
		}

	});

	mCommandQueue.push(command);
}

void World::updateMousePosition() {
	mouseTarget = mTarget.mapPixelToCoords(MousePos, mWorldView);

	Command playerGuider;
	playerGuider.category = Category::EnemyProjectile;
	playerGuider.action = derivedAction<Projectile>([this](Projectile& missile, sf::Time)
	{
		if (missile.guideSet) {
			return;
		}

		if (missile.isPlayerFollower()) {

			FOREACH(Vehicle* player, mPlayerAircrafts)
			{
				missile.guideTowards(player->getWorldPosition() + (player->getVelocity() * 50.f));
				missile.guideSet = true;
				break;
			}
		}
	});

	// Setup command that guides all missiles to the enemy which is currently closest to the player
	Command mouseGuider;
	mouseGuider.category = Category::AlliedProjectile;
	mouseGuider.action = derivedAction<Projectile>([this](Projectile& missile, sf::Time)
	{

		
		if (missile.guideSet) {
			return;
		}

		if (missile.isMouseFollower()) {
			missile.guideTowards(mouseTarget);
			missile.guideSet = true;
		}
	});

	// Push commands, reset active enemies
	mCommandQueue.push(mouseGuider);
	mCommandQueue.push(playerGuider);
}


sf::FloatRect World::getViewBounds() const
{
	return sf::FloatRect(mWorldView.getCenter() - mWorldView.getSize() / 2.f, mWorldView.getSize());
}

sf::FloatRect World::getBattlefieldBounds() const
{
	// Return view bounds + some area at top, where enemies spawn
	sf::FloatRect bounds = getViewBounds();
	bounds.top -= 100.f;
	bounds.height += 100.f;

	return bounds;
}
