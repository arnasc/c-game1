#pragma once
#include "Entity.hpp"
#include "ResourceIdentifiers.hpp"

#include <SFML/Graphics/Sprite.hpp>

class Projectile : public Entity
{
public:
	enum Type
	{
		AlliedBullet,
		EnemyBullet,
		Missile,
		EnemyToPlayerBullet,
		BikerBullet,
		MuscleCarBullet,
		HelicopterBullet,
		MouseBullet = BikerBullet | MuscleCarBullet | HelicopterBullet,
		TypeCount 
	};

public:
	Projectile(Type type, const TextureHolder& texture);

	void guideTowards(sf::Vector2f position);
	bool isGuided() const;
	bool guideSet = false;

	bool isMouseFollower() const;
	bool isPlayerFollower() const;

	virtual unsigned int getCategory() const;
	virtual sf::FloatRect getBoundingRect() const;
	float getMaxSpeed() const;
	int getDamage() const;
	sf::Vector2f mTargetDirection;

private:
	virtual void updateCurrent(sf::Time dt, CommandQueue& commands);
	virtual void drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const;

private:
	Type mType;
	sf::Sprite mSprite;

};