#include "DataTables.hpp"
#include "Aircraft.hpp"
#include "Projectile.hpp"
#include "Pickup.hpp"
#include "Particle.hpp"
#include "StaticSprite.h"

// For std::bind() placeholders _1, _2, ...
using namespace std::placeholders;

std::vector<AircraftData> initializeAircraftData()
{
	std::vector<AircraftData> data(Vehicle::TypeCount);

	data[Vehicle::Max].hitpoints = 2000;
	data[Vehicle::Max].speed = 200.f;
	data[Vehicle::Max].fireInterval = sf::seconds(.2f);
	data[Vehicle::Max].texture = Textures::Entities;
	data[Vehicle::Max].textureRect = sf::IntRect(0, 0, 48, 64);
	data[Vehicle::Max].hasRollAnimation = true;


	// BIKER
	data[Vehicle::Biker].hitpoints = 10;
	data[Vehicle::Biker].speed = 350.f;
	data[Vehicle::Biker].texture = Textures::Entities;
	data[Vehicle::Biker].textureRect = sf::IntRect(144, 0, 15, 32);
	data[Vehicle::Biker].directions.push_back(Direction(0.f, 0.f));
	data[Vehicle::Biker].fireInterval = sf::seconds(.2f);
	data[Vehicle::Biker].hasRollAnimation = false;

	// HELI
	data[Vehicle::Helicopter].hitpoints = 200;
	data[Vehicle::Helicopter].speed = -320.f;
	data[Vehicle::Helicopter].texture = Textures::Entities;
	data[Vehicle::Helicopter].textureRect = sf::IntRect(97, 105, 69, 79);
	data[Vehicle::Helicopter].directions.push_back(Direction(+20.f, 500.f));
	data[Vehicle::Helicopter].directions.push_back(Direction(-20.f, 500.f));
	data[Vehicle::Helicopter].fireInterval = sf::seconds(.2f);
	data[Vehicle::Helicopter].hasRollAnimation = false;
	data[Vehicle::Helicopter].hasSpriteAnimation = true;
	data[Vehicle::Helicopter].spriteFrames = 6;


	// MUSCLECAR
	data[Vehicle::MuscleCar].hitpoints = 60;
	data[Vehicle::MuscleCar].speed = -400.f;
	data[Vehicle::MuscleCar].texture = Textures::Entities;
	data[Vehicle::MuscleCar].textureRect = sf::IntRect(0, 104, 32, 65);

	data[Vehicle::MuscleCar].directions.push_back(Direction(+0.f, 1000.f));
	data[Vehicle::MuscleCar].directions.push_back(Direction(-10.f, 1000.f));
	data[Vehicle::MuscleCar].directions.push_back(Direction(+10.f, 1000.f));

	data[Vehicle::MuscleCar].fireInterval = sf::seconds(1.f);
	data[Vehicle::MuscleCar].hasRollAnimation = true;
	data[Vehicle::MuscleCar].animSpeed = 20;

	return data;
}

std::vector<ProjectileData> initializeProjectileData()
{
	std::vector<ProjectileData> data(Projectile::TypeCount);

	data[Projectile::AlliedBullet].damage = 10;
	data[Projectile::AlliedBullet].speed = 300.f;
	data[Projectile::AlliedBullet].texture = Textures::Entities;
	data[Projectile::AlliedBullet].textureRect = sf::IntRect(175, 64, 3, 14);

	data[Projectile::MouseBullet].damage = 10;
	data[Projectile::MouseBullet].speed = 5000.f;
	data[Projectile::MouseBullet].texture = Textures::Entities;
	data[Projectile::MouseBullet].textureRect = sf::IntRect(165, 32, 2, 18);

	data[Projectile::EnemyToPlayerBullet].damage = 10;
	data[Projectile::EnemyToPlayerBullet].speed = 1000.f;
	data[Projectile::EnemyToPlayerBullet].texture = Textures::Entities;
	data[Projectile::EnemyToPlayerBullet].textureRect = sf::IntRect(178, 64, 3, 14);

	data[Projectile::EnemyBullet].damage = 10;
	data[Projectile::EnemyBullet].speed = 300.f;
	data[Projectile::EnemyBullet].texture = Textures::Entities;
	data[Projectile::EnemyBullet].textureRect = sf::IntRect(178, 64, 3, 14);

	data[Projectile::Missile].damage = 200;
	data[Projectile::Missile].speed = 150.f;
	data[Projectile::Missile].texture = Textures::Entities;
	data[Projectile::Missile].textureRect = sf::IntRect(160, 64, 15, 32);

	return data;
}

std::vector<PickupData> initializePickupData()
{
	std::vector<PickupData> data(Pickup::TypeCount);

	data[Pickup::HealthRefill].texture = Textures::Entities;
	data[Pickup::HealthRefill].textureRect = sf::IntRect(0, 64, 40, 40);
	data[Pickup::HealthRefill].action = [](Vehicle& a) { a.repair(25); };

	data[Pickup::MissileRefill].texture = Textures::Entities;
	data[Pickup::MissileRefill].textureRect = sf::IntRect(40, 64, 40, 40);
	data[Pickup::MissileRefill].action = std::bind(&Vehicle::collectMissiles, _1, 3);

	data[Pickup::FireSpread].texture = Textures::Entities;
	data[Pickup::FireSpread].textureRect = sf::IntRect(80, 64, 40, 40);
	data[Pickup::FireSpread].action = std::bind(&Vehicle::increaseSpread, _1);

	data[Pickup::FireRate].texture = Textures::Entities;
	data[Pickup::FireRate].textureRect = sf::IntRect(120, 64, 40, 40);
	data[Pickup::FireRate].action = std::bind(&Vehicle::increaseFireRate, _1);

	return data;
}


std::vector<StaticSpriteData> initializeStaticSpriteData()
{
	std::vector<StaticSpriteData> data(Pickup::TypeCount);

	data[StaticSprite::WindowCharacter].texture = Textures::Entities;
	data[StaticSprite::WindowCharacter].textureRect = sf::IntRect(146, 36, 11, 10);

	data[StaticSprite::Gun].texture = Textures::Entities;
	data[StaticSprite::Gun].textureRect = sf::IntRect(144, 46, 21, 5);

	return data;
}


std::vector<ParticleData> initializeParticleData()
{

	std::vector<ParticleData> data(Particle::ParticleCount);

	data[Particle::Propellant].color = sf::Color(255, 255, 50);
	data[Particle::Propellant].lifetime = sf::seconds(0.6f);

	data[Particle::Smoke].color = sf::Color(50, 50, 50);
	data[Particle::Smoke].lifetime = sf::seconds(4.f);

	data[Particle::Dirt].color = sf::Color(57, 43, 43);
	data[Particle::Dirt].lifetime = sf::seconds(.1f);

	return data;
}

