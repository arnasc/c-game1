#include "Aircraft.hpp"
#include "DataTables.hpp"
#include "Utility.hpp"
#include "Pickup.hpp"
#include "CommandQueue.hpp"
#include "SoundNode.hpp"
#include "NetworkNode.hpp"
#include "ResourceHolder.hpp"
#include "EmitterNode.hpp"
#include "SoundPlayer.hpp"

#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderStates.hpp>

#include <cmath>

using namespace std::placeholders;

extern int gameScore;
extern int enemiesKilled;

namespace
{
	const std::vector<AircraftData> Table = initializeAircraftData();
}

Vehicle::Vehicle(Type type, const TextureHolder& textures, const FontHolder& fonts)
	: Entity(Table[type].hitpoints)
	, mType(type)
	, mSprite(textures.get(Table[type].texture), Table[type].textureRect)
	, mExplosion(textures.get(Textures::Explosion))
	, mPopText(textures.get(Textures::PopText))
	, mFireCommand()
	, mMissileCommand()
	, mMouseFireCommand()
	, mFireCountdown(sf::Time::Zero)
	, mIsFiring(false)
	, mIsLaunchingMissile(false)
	, mShowExplosion(true)
	, mExplosionBegan(false)
	, mShowPopText(true)
	, mPopTextBegan(false)
	, mSpawnedPickup(false)
	, mPickupsEnabled(false)
	, mFireRateLevel(1)
	, mSpreadLevel(1)
	, mMissileAmmo(2)
	, mDropPickupCommand()
	, mTravelledDistance(0.f)
	, mDirectionIndex(0)
	, mMissileDisplay(nullptr)
	, mIdentifier(0)
{

	initializeSounds();
	initializeTireParticles();
	animFrame = 0;
	lastAnimTime = sf::Time().Zero;
	//initializeScore();



	mExplosion.setFrameSize(sf::Vector2i(256, 256));
	mExplosion.setNumFrames(16);
	mExplosion.setDuration(sf::seconds(1));

	mPopText.setFrameSize(sf::Vector2i(128, 128));
	mPopText.setNumFrames(12);
	mPopText.setDuration(sf::seconds(1));

	setScale(2.f, 2.f);

	centerOrigin(mSprite);
	centerOrigin(mExplosion);
	centerOrigin(mPopText);

	mFireCommand.category = Category::SceneAirLayer;
	mFireCommand.action = [this, &textures](SceneNode& node, sf::Time)
	{
		createBullets(node, textures);
	};

	mEnemyToPlayerFireCommand.category = Category::SceneAirLayer;
	mEnemyToPlayerFireCommand.action = [this, &textures](SceneNode& node, sf::Time)
	{
		createEnemyToPlayerBullets(node, textures);
	};

	mMouseFireCommand.category = Category::SceneAirLayer;
	mMouseFireCommand.action = [this, &textures](SceneNode& node, sf::Time)
	{
		createMouseBullets(node, textures);
	};

	mMissileCommand.category = Category::SceneAirLayer;
	mMissileCommand.action = [this, &textures](SceneNode& node, sf::Time)
	{
		createProjectile(node, Projectile::Missile, 0.f, 0.5f, textures);
	};

	mDropPickupCommand.category = Category::SceneAirLayer;
	mDropPickupCommand.action = [this, &textures](SceneNode& node, sf::Time)
	{
		createPickup(node, textures);
	};

	std::unique_ptr<TextNode> healthDisplay(new TextNode(fonts, ""));
	mHealthDisplay = healthDisplay.get();
	attachChild(std::move(healthDisplay));

	//if (getCategory() == Category::PlayerAircraft)
	//{
	//	std::unique_ptr<TextNode> missileDisplay(new TextNode(fonts, ""));
	//	missileDisplay->setPosition(0, 70);
	//	mMissileDisplay = missileDisplay.get();
	//	attachChild(std::move(missileDisplay));
	//}

	updateTexts();
}

void Vehicle::initializeTireParticles() {
	float x1 = getBoundingRect().width / 2.f;
	float x2 = x1 * -1;

	float y = getBoundingRect().height - 10;

	std::unique_ptr<EmitterNode> dirtLeft(new EmitterNode(Particle::Dirt));
	std::unique_ptr<EmitterNode> dirtRight(new EmitterNode(Particle::Dirt));

	switch (mType) {
	case(Type::Max):
		x1 -= 35;
		x2 += 35;
		break;
	case(Type::Helicopter):
		return;
		break;
	default:
		x1 -= 5;
		x2 += 5;
		break;

	}

	dirtLeft->setPosition(x1, y / 2.f);
	dirtRight->setPosition(x2, y / 2.f);
	attachChild(std::move(dirtLeft));
	attachChild(std::move(dirtRight));
}
int Vehicle::getMissileAmmo() const
{
	return mMissileAmmo;
}

void Vehicle::setMissileAmmo(int ammo)
{
	mMissileAmmo = ammo;
}

void Vehicle::drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const
{

	if (isDestroyed() && mShowExplosion)
	{
		target.draw(mExplosion, states);
		if (!isAllied()) {
			target.draw(mPopText, states);
		}
	}
	else
	{
		target.draw(mSprite, states);
	}
}

void Vehicle::disablePickups()
{
	mPickupsEnabled = false;
}

void Vehicle::updateCurrent(sf::Time dt, CommandQueue& commands)
{
	// Update texts and roll animation
	updateTexts();
	updateAnimation(dt);

	if (!soundPlaying && sound != SoundEffect::None && !isDestroyed()) {
		playLocalSound(commands, sound, true);
		soundPlaying = true;
	}

	// Entity has been destroyed: Possibly drop pickup, mark for removal
	if (isDestroyed())
	{
		if (!scoreApplied) {
			enemiesKilled++;

			switch (mType) {
			case Vehicle::MuscleCar:
				gameScore += 100;
				break;
			case Vehicle::Helicopter:
				gameScore += 300;
				break;
			case Vehicle::Biker:
				gameScore += 500;
				break;
			}
			scoreApplied = true;
		}

		
		if (soundPlaying) {
			stopSounds(commands);
			soundPlaying = false;
		}
		

		// no pickups
		//checkPickupDrop(commands);
		mExplosion.update(dt);
		mPopText.update(dt);

		// Play explosion sound only once
		if (!mExplosionBegan)
		{
			SoundEffect::ID soundEffect = (randomInt(2) == 0) ? SoundEffect::Explosion1 : SoundEffect::Explosion2;
			playLocalSound(commands, soundEffect);
		
			if (!isAllied()) {
				int random = rand()%24;
				if (random < 12)
				{
					switch (random)
					{
					case 0:
						playLocalSound(commands, SoundEffect::VoiceChewGum);
						break;

					case 1:
						playLocalSound(commands, SoundEffect::VoiceEatShit);
						break;

					case 2:
						playLocalSound(commands, SoundEffect::VoiceJusticeServed);
						break;

					case 3:
						playLocalSound(commands, SoundEffect::VoiceMaxJustice);
						break;

					case 4:
						playLocalSound(commands, SoundEffect::VoiceServed);
						break;

					case 5:
						playLocalSound(commands, SoundEffect::VoiceTasteJustice);
						break;

					case 6:
						playLocalSound(commands, SoundEffect::VioceCrime);
						break;

					case 7:
						playLocalSound(commands, SoundEffect::VoiceCapital);
						break;

					case 8:
						playLocalSound(commands, SoundEffect::VoiceJudge);
						break;

					case 9:
						playLocalSound(commands, SoundEffect::VoiceJusticeMax);
						break;

					case 10:
						playLocalSound(commands, SoundEffect::VoiceSmell);
						break;

					case 11:
						playLocalSound(commands, SoundEffect::VoiceTasteGood);
						break;
					}
				}
			}
			// Emit network game action for enemy explosions
			if (!isAllied())
			{
				sf::Vector2f position = getWorldPosition();

				Command command;
				command.category = Category::Network;
				command.action = derivedAction<NetworkNode>([position](NetworkNode& node, sf::Time)
				{
					node.notifyGameAction(GameActions::EnemyExplode, position);
				});

				commands.push(command);
			}

			mExplosionBegan = true;
			mPopTextBegan = true;
		}
		return;
	}

	// Check if bullets or missiles are fired
	checkProjectileLaunch(dt, commands);

	// Update enemy movement pattern; apply velocity
	updateMovementPattern(dt);
	Entity::updateCurrent(dt, commands);
}

unsigned int Vehicle::getCategory() const
{
	if (isAllied())
		return Category::PlayerAircraft;
	else
		return Category::EnemyAircraft;
}

sf::FloatRect Vehicle::getBoundingRect() const
{
	return getWorldTransform().transformRect(mSprite.getGlobalBounds());
}

bool Vehicle::isMarkedForRemoval() const
{
	return isDestroyed() && ((mExplosion.isFinished()&& mPopText.isFinished()) || (!mShowExplosion&&!mShowPopText));
}

void Vehicle::remove()
{
	enemiesKilled++;
	Entity::remove();
	mShowExplosion = false;
	mShowPopText = false;
}

bool Vehicle::isAllied() const
{
	return mType == Max;
}

float Vehicle::getMaxSpeed() const
{
	return Table[mType].speed;
}

void Vehicle::increaseFireRate()
{
	if (mFireRateLevel < 10)
		++mFireRateLevel;
}

void Vehicle::increaseSpread()
{
	if (mSpreadLevel < 3)
		++mSpreadLevel;
}

void Vehicle::collectMissiles(unsigned int count)
{
	mMissileAmmo += count;
}

void Vehicle::mouseFire()
{
	// Only ships with fire interval != 0 are able to fire
	if (Table[mType].fireInterval != sf::Time::Zero)
		mIsMouseFiring = true;
}

void Vehicle::initializeSounds()
{
	switch (mType) {
	case Helicopter:
		sound = SoundEffect::Helicopter;
		break;
	default:
		sound = SoundEffect::None;
		break;
	}


}

void Vehicle::fire()
{
	// Only ships with fire interval != 0 are able to fire
	if (Table[mType].fireInterval != sf::Time::Zero)
		mIsFiring = true;
}

void Vehicle::launchMissile()
{
	if (mMissileAmmo > 0)
	{
		mIsLaunchingMissile = true;
		--mMissileAmmo;
	}
}

void Vehicle::stopSounds(CommandQueue& commands)
{
	Command command;
	command.category = Category::SoundEffect;
	command.action = derivedAction<SoundNode>(
		[](SoundNode& node, sf::Time)
	{
		node.stopSounds();
	});

	commands.push(command);
}

void Vehicle::playLocalSound(CommandQueue& commands, SoundEffect::ID effect, bool loop)
{
	sf::Vector2f worldPosition = getWorldPosition();

	Command command;
	command.category = Category::SoundEffect;
	command.action = derivedAction<SoundNode>(
		[effect, worldPosition, loop](SoundNode& node, sf::Time)
	{
		node.playSound(effect, worldPosition, loop);
	});

	commands.push(command);
}

void Vehicle::playLocalSound(CommandQueue& commands, SoundEffect::ID effect)
{
	playLocalSound(commands, effect, false);
}

int	Vehicle::getIdentifier()
{
	return mIdentifier;
}

void Vehicle::setIdentifier(int identifier)
{
	mIdentifier = identifier;
}

void Vehicle::updateMovementPattern(sf::Time dt)
{
	// Enemy airplane: Movement pattern
	const std::vector<Direction>& directions = Table[mType].directions;
	if (!directions.empty())
	{
		// Moved long enough in current direction: Change direction
		if (abs(mTravelledDistance) > directions[mDirectionIndex].distance)
		{
			mDirectionIndex = (mDirectionIndex + 1) % directions.size();
			mTravelledDistance = 0.f;
		}

		// Compute velocity from direction
		float radians = toRadian(directions[mDirectionIndex].angle + 90.f);
		float vx = getMaxSpeed() * std::cos(radians);
		float vy = getMaxSpeed() * std::sin(radians);

		setVelocity(vx, vy);

		mTravelledDistance += getMaxSpeed() * dt.asSeconds();
	}
}

void Vehicle::checkPickupDrop(CommandQueue& commands)
{
	if (!isAllied() && randomInt(3) == 0 && !mSpawnedPickup)
		commands.push(mDropPickupCommand);

	mSpawnedPickup = true;
}

void Vehicle::checkProjectileLaunch(sf::Time dt, CommandQueue& commands)
{
	// Enemies try to fire all the time
	if (!isAllied())
		fire();

	// Check for automatic gunfire, allow only in intervals
	if (mIsFiring && mFireCountdown <= sf::Time::Zero)
	{
		// Interval expired: We can fire a new bullet

		if (mType == Vehicle::Helicopter || mType == Vehicle::MuscleCar || mType == Vehicle::Biker) {
			commands.push(mEnemyToPlayerFireCommand);
		}
		else {
			commands.push(mFireCommand);
		}

		playLocalSound(commands, isAllied() ? SoundEffect::AlliedGunfire : SoundEffect::EnemyGunfire);
		mFireCountdown += Table[mType].fireInterval / (mFireRateLevel + 1.f);
		mIsFiring = false;
	}

	if (mIsMouseFiring && mFireCountdown <= sf::Time::Zero) {
		commands.push(mMouseFireCommand);
		playLocalSound(commands, isAllied() ? SoundEffect::AlliedGunfire : SoundEffect::EnemyGunfire);

		mFireCountdown += Table[mType].fireInterval / (mFireRateLevel + 1.f);
		mIsMouseFiring = false;
	}
	else if (mFireCountdown > sf::Time::Zero)
	{
		// Interval not expired: Decrease it further
		mFireCountdown -= dt;
		mIsFiring = false;
		mIsMouseFiring = false;
	}


	// Check for missile launch
	//if (mIsLaunchingMissile)
	//{
	//	commands.push(mMissileCommand);
	//	playLocalSound(commands, SoundEffect::LaunchMissile);

	//	mIsLaunchingMissile = false;
	//}
}

void Vehicle::createEnemyToPlayerBullets(SceneNode& node, const TextureHolder& textures) const
{
	Projectile::Type type = Projectile::Type::EnemyToPlayerBullet;
	createProjectile(node, type, -0.3f, 0.f, textures);
}

void Vehicle::createMouseBullets(SceneNode& node, const TextureHolder& textures) const
{
	Projectile::Type type = Projectile::Type::MouseBullet;

	switch (mSpreadLevel)
	{
	case 1:
		createProjectile(node, type, -0.3f, 0.f, textures);
		break;

	//case 2:
	//	createProjectile(node, type, -0.33f, 0.33f, textures);
	//	createProjectile(node, type, +0.33f, 0.33f, textures);
	//	break;

	//case 3:
	//	createProjectile(node, type, -0.5f, 0.33f, textures);
	//	createProjectile(node, type, 0.0f, 0.5f, textures);
	//	createProjectile(node, type, +0.5f, 0.33f, textures);
	//	break;
	}
}

void Vehicle::createBullets(SceneNode& node, const TextureHolder& textures) const
{
	Projectile::Type type = isAllied() ? Projectile::AlliedBullet : Projectile::EnemyBullet;

	switch (mSpreadLevel)
	{
	case 1:
		createProjectile(node, type, 0.0f, 0.5f, textures);
		break;

	case 2:
		createProjectile(node, type, -0.33f, 0.33f, textures);
		createProjectile(node, type, +0.33f, 0.33f, textures);
		break;

	case 3:
		createProjectile(node, type, -0.5f, 0.33f, textures);
		createProjectile(node, type, 0.0f, 0.5f, textures);
		createProjectile(node, type, +0.5f, 0.33f, textures);
		break;
	}
}

void Vehicle::createProjectile(SceneNode& node, Projectile::Type type, float xOffset, float yOffset, const TextureHolder& textures) const
{
	std::unique_ptr<Projectile> projectile(new Projectile(type, textures));

	sf::Vector2f offset(xOffset * mSprite.getGlobalBounds().width, yOffset * mSprite.getGlobalBounds().height);
	sf::Vector2f velocity(0, projectile->getMaxSpeed());

	float sign = isAllied() ? -1.f : +1.f;
	projectile->setPosition(getWorldPosition() + offset * sign);
	projectile->setVelocity(velocity * sign);
	node.attachChild(std::move(projectile));
}

void Vehicle::createPickup(SceneNode& node, const TextureHolder& textures) const
{
	auto type = static_cast<Pickup::Type>(randomInt(Pickup::TypeCount));

	std::unique_ptr<Pickup> pickup(new Pickup(type, textures));
	pickup->setPosition(getWorldPosition());
	pickup->setVelocity(0.f, 1.f);
	node.attachChild(std::move(pickup));
}

void Vehicle::updateTexts()
{
	// Display hitpoints
	if (isDestroyed())
		mHealthDisplay->setString("");
	else
		mHealthDisplay->setString(toString(getHitpoints()) + " HP");
	mHealthDisplay->setPosition(0.f, 50.f);
	mHealthDisplay->setRotation(-getRotation());

	// Display missiles, if available
	/*if (mMissileDisplay)
	{
		if (mMissileAmmo == 0 || isDestroyed())
			mMissileDisplay->setString("");
		else
			mMissileDisplay->setString("M: " + toString(mMissileAmmo));
	}*/
}

void Vehicle::updateAnimation(sf::Time dt)
{
	if (Table[mType].hasRollAnimation)
	{
		sf::IntRect textureRect = Table[mType].textureRect;

		float velocityX = getVelocity().x;

		if (abs(velocityX) < .1f) {
			mSprite.setTextureRect(textureRect);
			return;
		}

		// Roll left: Texture rect offset once
		if (velocityX < 0.f)
			textureRect.left += textureRect.width;

		// Roll right: Texture rect offset twice
		else if (velocityX > 0.f)
			textureRect.left += 2 * textureRect.width;

		mSprite.setTextureRect(textureRect);
	}
	else if (Table[mType].hasSpriteAnimation) {
		int time = lastAnimTime.asMilliseconds();
		if (lastAnimTime.asMilliseconds() > Table[mType].animSpeed) {
			sf::IntRect textureRect = Table[mType].textureRect;

			if (animFrame == Table[mType].spriteFrames) {
				animFrame = 0;
			}

			textureRect.left += textureRect.width * animFrame;
			animFrame++;
			lastAnimTime = sf::Time().Zero;

			mSprite.setTextureRect(textureRect);
		}
		lastAnimTime += dt;
	}
}