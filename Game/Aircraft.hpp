#pragma once
#include "Entity.hpp"
#include "Command.hpp"
#include "ResourceIdentifiers.hpp"
#include "Projectile.hpp"
#include "TextNode.hpp"
#include "Animation.hpp"

#include <SFML/Window/Mouse.hpp>
#include <SFML/Graphics/Sprite.hpp>


class Vehicle : public Entity
{
public:
	enum Type
	{
		Max,
		Biker,
		Avenger,
		Helicopter,
		MuscleCar,
		TypeCount
	};


public:
	Vehicle(Type type, const TextureHolder& textures, const FontHolder& fonts);

	virtual unsigned int	getCategory() const;
	virtual sf::FloatRect	getBoundingRect() const;
	virtual void			remove();
	virtual bool 			isMarkedForRemoval() const;
	bool					isAllied() const;
	float					getMaxSpeed() const;
	void					disablePickups();
	Type					mType;
	bool					scoreApplied = false;

	void					increaseFireRate();
	void					increaseSpread();
	void					collectMissiles(unsigned int count);

	void					mouseFire();
	void					initializeSounds();
	SoundEffect::ID			sound;
	bool					soundPlaying = false;

	void 					fire();
	void					launchMissile();
	void					stopSounds(CommandQueue& commands);
	void					playLocalSound(CommandQueue & commands, SoundEffect::ID effect, bool loop);
	void					playLocalSound(CommandQueue& commands, SoundEffect::ID effect);
	int						getIdentifier();
	void					setIdentifier(int identifier);
	void					initializeTireParticles();
	int						getMissileAmmo() const;
	void					setMissileAmmo(int ammo);


private:
	virtual void			drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const;
	virtual void 			updateCurrent(sf::Time dt, CommandQueue& commands);
	void					updateMovementPattern(sf::Time dt);
	void					checkPickupDrop(CommandQueue& commands);
	void					checkProjectileLaunch(sf::Time dt, CommandQueue& commands);

	void					createEnemyToPlayerBullets(SceneNode & node, const TextureHolder & textures) const;
	void					createMouseBullets(SceneNode & node, const TextureHolder & textures) const;
	void					createBullets(SceneNode& node, const TextureHolder& textures) const;

	void					createPlayerFollowingProjectile(SceneNode & node, Projectile::Type type, float xOffset, float yOffset, const TextureHolder & textures) const;
	void					createMouseFollowingProjectile(SceneNode & node, Projectile::Type type, float xOffset, float yOffset, const TextureHolder & textures) const;
	void					createProjectile(SceneNode& node, Projectile::Type type, float xOffset, float yOffset, const TextureHolder& textures) const;

	void					createPickup(SceneNode& node, const TextureHolder& textures) const;

	void					updateTexts();
	void					updateAnimation(sf::Time dt);




private:

	sf::Sprite				mSprite;
	Animation				mExplosion;
	Animation				mPopText;
	Command					mMouseFireCommand;
	Command					mEnemyToPlayerFireCommand;
	Command 				mFireCommand;
	Command					mMissileCommand;
	sf::Time				mFireCountdown;
	bool 					mIsFiring;
	bool 					mIsMouseFiring;
	bool					mIsLaunchingMissile;
	bool 					mShowExplosion;
	bool					mExplosionBegan;
	bool 					mShowPopText;
	bool					mPopTextBegan;
	bool					mPlayedExplosionSound;
	bool					mSpawnedPickup;
	bool					mPickupsEnabled;

	int						mFireRateLevel;
	int						mSpreadLevel;
	int						mMissileAmmo;

	Command 				mDropPickupCommand;
	float					mTravelledDistance;
	std::size_t				mDirectionIndex;
	TextNode*				mHealthDisplay;
	TextNode*				mMissileDisplay;


	int						animFrame;
	sf::Time				lastAnimTime;

	int						mIdentifier;
};