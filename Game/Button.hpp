#pragma once
#include "Component.hpp"
#include "ResourceIdentifiers.hpp"
#include "State.hpp"

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>

#include <vector>
#include <string>
#include <memory>
#include <functional>

class SoundPlayer;

namespace GUI
{

	class Button : public Component
	{
	public:
		typedef std::shared_ptr<Button>		Ptr;
		typedef std::function<void()>		Callback;

		enum Type
		{
			PlayNormal,
			SettingsNormal,
			QuitNormal,
			PlaySelected,
			SettingsSelected,
			QuitSelected,
			PlayPressed,
			SettingsPressed,
			QuitPressed,
			SoundNormal,
			MusicNormal,
			BackNormal,
			SoundSelected,
			MusicSelected,
			BackSelected,
			ButtonCount,
		};


	public:
		Button(State::Context context);

		void					setCallback(Callback callback);
		void					setText(const std::string& text);
		void					setToggle(bool flag);
		void					checkNormalTexture(std::string text);
		void					checkSelectedTexture(std::string text);

		virtual bool			isSelectable() const;
		virtual void			select();
		virtual void			deselect();

		virtual void			activate();
		virtual void			deactivate();

		virtual void			handleEvent(const sf::Event& event);

		void					changeTexture(Type buttonType);

	private:
		virtual void			draw(sf::RenderTarget& target, sf::RenderStates states) const;
	

	private:
		Callback				mCallback;
		sf::Sprite				mSprite;
		sf::Text				mText;
		bool					mIsToggle;
		SoundPlayer&			mSounds;
	};

}