#include "SoundNode.hpp"
#include "SoundPlayer.hpp"

SoundNode::SoundNode(SoundPlayer& player)
	:SceneNode()
	,mSounds(player)
{

}

void SoundNode::stopSounds()
{
	mSounds.stop();
}

void SoundNode::playSound(SoundEffect::ID sound, sf::Vector2f position, bool loop)
{
	mSounds.play(sound, position, loop);
}

void SoundNode::playSound(SoundEffect::ID sound, sf::Vector2f position)
{
	mSounds.play(sound, position);
}

unsigned int SoundNode::getCategory() const
{
	return Category::SoundEffect;
}